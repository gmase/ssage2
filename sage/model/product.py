from marshmallow import Schema, fields, post_load


class Product():
  def __init__(self, gender_names,category_names,currency,size_infos,country_code,title,base_sku,_current_price_value,timestamp,brand,image_urls,description_text,_original_price_value,url,color_name,identifier,discount):
    # ,discount
    self.gender_names = gender_names
    self.category_names = category_names
    self.currency = currency
    self.size_infos = size_infos
    self.country_code = country_code
    self.title = title
    self.base_sku = base_sku
    self._current_price_value = _current_price_value
    self.timestamp = timestamp
    self.brand = brand
    self.image_urls = image_urls
    self.description_text = description_text
    self._original_price_value = _original_price_value
    self.url = url
    self.color_name = color_name
    self.identifier = identifier
    self.discount = discount
    # self.discount = float(_original_price_value) - float(_current_price_value)

  def __repr__(self):
    return '<Product(name={self.identifier!r})(discount={self.discount!r})>'.format(self=self)

  @staticmethod
  def cmp_price(item):
    return item._current_price_value

  @staticmethod
  def cmp_discount(item):
    return item.discount

  def __eq__(self, other):
    return self.identifier == other.identifier

  def __lt__(self, other):
    return self.identifier < other.identifier


class ProductSchema(Schema):
    gender_names = fields.Str()
    category_names = fields.Str()
    currency = fields.Str()
    size_infos = fields.Str()
    country_code = fields.Str()
    title = fields.Str()
    base_sku = fields.Str()
    _current_price_value = fields.Number()
    timestamp = fields.Str()
    brand = fields.Str()
    image_urls = fields.Str()
    description_text = fields.Str()
    _original_price_value = fields.Number()
    url = fields.Str()
    color_name = fields.Str()
    identifier = fields.Str()
    discount = fields.Number()

    @post_load
    def make_product(self, data):
      return Product(**data)
