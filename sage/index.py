from flask import Flask, jsonify, request
import pandas as pd
import heapq as hq
from datetime import datetime
import csv
import re

from sage.model.product import Product, ProductSchema
app = Flask(__name__)

# This is slower for our input size, might be better for bigger inputs
def get_top_with_heap(x, file):
  start_time = datetime.now()
  h = []
  with open(file, 'r') as f:
    reader = csv.reader(f)
    reader.__next__()
    current_size = 0
    for row in reader:
        item = Product(*row[1:])
        if current_size < x:
            hq.heappush(h, [float(item.discount) * -1, item])
            current_size += 1
        elif (item.discount * -1) < h[0][0]:
            hq.heappushpop(h, [item.discount * -1, item])
  print ('Elapsed time heap sort: {}'.format(datetime.now() - start_time))
  return h

def get_top_with_pandas(x, file):
    products = []
    start_time = datetime.now()
    df = pd.read_csv (filepath_or_buffer = file)
    # Order by new column discount
    discount = (df._original_price_value - df._current_price_value)
    df['discount'] = discount
    top_x = df.sort_values('discount', ascending=False).head(x)
    j = 0
    for i in top_x.to_records(index = False):
        products.append(Product(*(i.tolist()[1:])))
    print ('Elapsed time pandas sort: {}'.format(datetime.now() - start_time))
    return products,df

# print(get_top_with_heap(20 ,'products.csv'))
products, all_products = get_top_with_pandas(20 ,'products.csv')

@app.route('/by_price')
def get_products_by_price():
  only_20 = sorted(products[:min(len(products), 20)], key = Product.cmp_price)
  schema = ProductSchema(many=True)
  prods = schema.dump(
    only_20
  )
  return jsonify(prods.data)

@app.route('/by_discount')
def get_products_by_discount():
  only_20 = sorted(products[:min(len(products), 20)], key = Product.cmp_discount)
  schema = ProductSchema(many=True)
  prods = schema.dump(
    only_20
  )
  return jsonify(prods.data)

@app.route('/top_discounted')
def get_top_discounted():
  # no sorting required
  only_20 = products[:min(len(products), 20)]
  schema = ProductSchema(many=True)
  prods = schema.dump(
    only_20
  )
  return jsonify(prods.data)

@app.route('/by_color/<color>')
def get_by_color(color):
  # no sorting required
  color_prods = []
  filter_expresion = re.compile(color, re.IGNORECASE)
  selected_color = all_products[all_products['color_name'].str.contains(filter_expresion, na=False)]
  for i in selected_color.to_records(index = False):
    color_prods.append(Product(*(i.tolist()[1:])))
  schema = ProductSchema(many=True)
  prods = schema.dump(
    color_prods
  )
  return jsonify(prods.data)

if __name__ == "__main__":
    app.run()
