### To start api service
pipenv shell
./bootstrap.sh

### Api calls:
1. Give me 20 products ordered by price: curl http://localhost:5000/by_price
1. Give me 20 products ordered by discount. curl http://localhost:5000/by_discount
1. Give me the most discounted 20 products. curl http://localhost:5000/top_discounted
1. Give me all products with color red or any other color. curl http://localhost:5000/by_color/<color>
